<?php
namespace Admin\Model;

use Admin\Model\Magic;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Update;
use Zend\Db\Adapter\Adapter;

 class Admin
 {
 	protected $adapter;
 	protected $table = 'users';

 	function __construct()
 	{
 		$dbdata = new Magic();
        $this->adapter = $dbdata->getDbCon();
 	
 	}


     public function adminLogin($email,$password)
     {

         $sql = new Sql($this->adapter); 
         $select = new Select($this->table);
         $select->where(array('email' =>$email,'pwd' =>$password,'type'=>'ADMIN'));
         $selectString = $sql->getSqlStringForSqlObject($select);
         $row =  $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->current();      
         if (!$row) {
           $mesg = 1; 
         }else{
           $mesg = $email;
         }
         return $mesg;

     }

    public function clinicListing()
     {
        $where = array('approved =  0');
        
        $sql = new Sql($this->adapter);
        $select = new Select('clinics');
        $select->where($where);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $clinics =  $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);  
        return $clinics;  
     }

      public function clinicLogs()
     {
        $where = array('approved =  0');
        
        $sql = new Sql($this->adapter);
        $select = new Select('cliniclogs');
        $select->where($where);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $cliniclogs =  $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);  
        return $cliniclogs;  
     }

      public function imageListing()
     {
        $where = array('approved =  0');
        
        $sql = new Sql($this->adapter);
        $select = new Select('images');
        $select->where($where);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $images =  $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);  
        return $images;  
     }

     public function reviewsListing()
     {
        $where = array('approved =  0');
        
        $sql = new Sql($this->adapter);
        $select = new Select('cliniccomments');
        $select->where($where);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $reviews =  $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);  
        return $reviews;  
     }

     public function ApproveClinic($pid)
     {
        $sql  = new Sql( $this->adapter );
        $update = $sql->update();
        $update->table('clinics');
        $newData = array(
        'approved'=> 1
        );
        $update->set( $newData );
        $update->where( array( 'id' => $pid ) );

        $statement  = $sql->prepareStatementForSqlObject( $update );
        $results    = $statement->execute();
     }
     


 }