<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;      
use Admin\Form\LoginForm;   
use Zend\Session\Container;
use Admin\Model\Admin;
use Zend\Session\SessionManager;
use Zend\Mvc\Controller\Plugin\Redirect;



class AdminController extends AbstractActionController
{

    public function indexAction() // HOME PAGE
    {
       $form = new LoginForm();
       $form->get('submit')->setValue('Login');
       $this->layout()->setVariable('myTitle', 'Admin Login');
       $request = $this->getRequest();
       if(isset($sessionContainer->myVar)){
         $admin_email = $sessionContainer->myVar;
       }else{
         $admin_email = '';
       }
		    if($request->isPost()){

				$res = $request->getPost();
				$password = $res['password'];
				$email = $res['email'];
				$data = new Admin();
                $result =  $data->adminLogin($email,$password);
                  
                if($result!=1){
                     	$sessionManager = new SessionManager();
                        $sessionContainer = new Container('ContainerNamespace', $sessionManager);
                     	$sessionContainer->myVar = $result;
                        $this->layout()->setVariable('admin_login', $sessionContainer->myVar);	
                      return array('form' => $form,'email'=>$sessionContainer->myVar);
                }else{
                 	 return array('form' => $form,'messsage'=>'Admin not found');
                }
					
			}else{
				 return array('form' => $form,'email'=>$admin_email);
			}  

       }

       public function reviewAction(){
       	  $sessionManager = new SessionManager();
                    $sessionContainer = new Container('ContainerNamespace', $sessionManager);

          $data = new Admin();
           return new ViewModel(array(
             'reviews' => $data->reviewsListing(),
             'email'=>$sessionContainer->myVar,
         ));                 
       }

       public function welcomeAction(){
       	  $sessionManager = new SessionManager();
                    $sessionContainer = new Container('ContainerNamespace', $sessionManager);

           $data = new Admin();
           return new ViewModel(array(
             'clinics' => $data->clinicListing(),
             'cliniclogs' => $data->clinicLogs(),
             'images' => $data->imageListing(),
             'email'=>$sessionContainer->myVar,
         ));       
       }

       public function compareAction(){
       	  $sessionManager = new SessionManager();
                    $sessionContainer = new Container('ContainerNamespace', $sessionManager);
         
          return array('email'=>$sessionContainer->myVar);
       }

       public function logoutAction(){
       	  $sessionManager = new SessionManager();
                    $sessionContainer = new Container('ContainerNamespace', $sessionManager);
          unset($sessionContainer->myVar);
          return array('message' => 'Logout successfully');
       }

       public function approveAction(){
       $request = $this->getRequest();
       	 if($request->isPost()){

				$res = $request->getPost();
				$pid = $res['pid'];
				$data = new Admin();
                $result =  $data->ApproveClinic($pid);

               $this->redirect('welcome');
			}

       }

       public function disapproveAction(){
       $request = $this->getRequest();
       	 if($request->isPost()){

				$res = $request->getPost();
				$pid = $res['pid'];
				$data = new Admin();
                $result =  $data->DiaspproveClinic($pid);
                $this->_redirect('admin/welcome');
			}

       }


}
