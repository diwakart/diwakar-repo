<?php
namespace Application\Model;

use Application\Model\Magic;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Update;
use Zend\Db\Adapter\Adapter;


 class Addlisting
 {
 	protected $adapter;
 	protected $table = 'clinics';

 	function __construct()
 	{
 		$dbdata = new Magic();
        $this->adapter = $dbdata->getDbCon();
 	
 	}

    public function addClinic($name,$address,$address2,$city,$country,$state,$postal,$contact_name,$phone,$fax,$email,$website,$type,$details,$has_medical,$has_dental,$services,$remarks)
     {
 		$sql = new Sql($this->adapter); 
        $insert = $sql->insert($this->table);
        $newData = array(
        'name'=> $name,
        'address'=> $address,
        'address2'=> $address2,
        'city'=> $city,
        'county'=> $country,
        'state'=> $state,
        'postal'=> $postal,
        'contact_name'=> $contact_name,
        'phone'=> $phone,
        'fax'=> $fax,
        'email'=> $email,
        'website'=> $website,
        'type'=> $type,
        'details'=> $details,
        'has_medical'=> $has_medical,
        'has_dental'=> $has_dental,
        'services'=> $services,
        'remarks'=> $remarks,
        );
        $insert->values($newData);
        $selectString = $sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        return  $results;

     }

 }