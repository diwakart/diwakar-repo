<?php
namespace Application\Model;

use Application\Model\Magic;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\Adapter\Adapter;

 class User
 {
    protected $adapter;
    protected $table = 'users';

    function __construct()
    {
        $dbdata = new Magic();
        $this->adapter = $dbdata->getDbCon();
    
    }

    public function dosignup($data)
    {
        $sql = new Sql($this->adapter); 
        $insert = $sql->insert($this->table);
        $newData = array(
        'id'=> '',
        'firstname'=> $data['firstname'],
        'pwd'=> $data['password'],
        'email'=> $data['email'],
        'created'=> date('Y-m-d h:s:m'),
        'login'=> '',
        'status'=> '',
        'lastname'=> $data['lastname'],
        'city'=> $data['city'],
        'state'=> $data['state'],
        'zip'=> $data['zipcode'],
        'ip_address'=> $_SERVER['REMOTE_ADDR'],
        'property_id'=> '',
        'resetcode'=> '',
        'attempt'=> '',
        'logintime'=> '',
        'type'=> '',
        'address'=> '',
        'receive_email'=> '',
        );
        $insert->values($newData);
        $selectString = $sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        return  $results;



    }
 }