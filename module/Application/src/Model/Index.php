<?php
namespace Application\Model;

use Application\Model\Magic;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Update;
use Zend\Db\Adapter\Adapter;


 class Index
 {
 	protected $adapter;
 	protected $table = 'clinics';

 	function __construct()
 	{
 		$dbdata = new Magic();
        $this->adapter = $dbdata->getDbCon();
 	
 	}

    public function getClinics()
     {
        $where = array('approved =  1', "details <> '' ");
        
        $sql = new Sql($this->adapter);
        $select = new Select($this->table);
        $select->where($where);
        $select->limit(5);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $clinics =  $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);  
        return $clinics;  
     }


 }