<?php
namespace Application\Model;

use Application\Model\Magic;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Update;
use Zend\Db\Adapter\Adapter;


 class Resources
 {
 	protected $adapter;
 	protected $table = 'resources';

 	function __construct()
 	{
 		$dbdata = new Magic();
        $this->adapter = $dbdata->getDbCon();
 	
 	}

    public function getResourcesTable()
     {
 		$sql = new Sql($this->adapter);
        $select = new Select($this->table);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $resources =  $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);      
        return $resources;  
     }
     public function getCategoriesTable()
     {
 		$sql = new Sql($this->adapter);
        $select = new Select('resourcecategory');
        $selectString = $sql->getSqlStringForSqlObject($select);
        $categories =  $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        return $categories;  
     }

 }