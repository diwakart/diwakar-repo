<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
use Application\Model\User;       
use Application\Form\SignupForm;   
use Application\Form\UserForm;
use Zend\Authentication\AuthenticationService;

class UserController extends AbstractActionController
{


    public function indexAction() {
      //die("test");
    	$data = $this->getRequest()->getPost();
    	$email = $data['email'];
    	$password = $data['password'];
    	echo "Your credential : <br> Username (email) : ".$email."<br>Password : ".$password."";
    	die();
    	//echo "Pass".$password."<br>email".$email."<br>";
	}

	public function loginAction()
	{
       $form = new UserForm();
       $form->get('submit')->setValue('Login');

       $this->layout()->setVariable('myTitle', 'Member Login');
        $auth = new AuthenticationService();

        if (!$auth->hasIdentity()) 
        {
            $identity = $auth->getIdentity();
        }


	  	   
         return array('form' => $form);
	}

  public function signupAction()
  {      
         $this->layout()->setVariable('myTitle', 'Create New User');
         $form = new SignupForm();
         $form->get('submit')->setValue('Register');


          if ($this->getRequest()->isPost())
          {
             $data = $this->getRequest()->getPost();
             $userModel = new User();
             $Result = $userModel->dosignup($data);
             // return array('form' => $form);
           }
         return array('form' => $form);
  }

}