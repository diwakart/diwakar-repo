<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Model\Contact;       
use Application\Form\ContactForm;  
use Zend\Mail;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

class ContactController extends AbstractActionController
{
    public function indexAction()
    {

    	 $this->layout()->setVariable('myTitle', 'Contact');
    	 $form = new ContactForm();

		  if ($this->getRequest()->isPost())
			{
				$req = $this->getRequest();
				$data = $req->getPost();
		    	$this->sendEmail($data);
			}
         $form->get('submit')->setValue('Send e-mail');
         return array('form' => $form);
    }

     public function sendEmail($data)
    {
		$message = new Message();
		$message->addTo("support@freeclinicdirectory.org")
		        ->addFrom($data['email'],$data['name'])
		        ->setSubject($data['subject'])
		        ->setBody($data['message']);

		$transport = new SmtpTransport();
		$options   = new SmtpOptions(array(
			'name'				=> 'mail.freeclinicdirectory.org',
		    'host'              => 'mail.freeclinicdirectory.org',
		    'port'              =>  587, // Notice port change for TLS is 587
		    'connection_class'  => 'login',
		    'connection_config' => array(
					        'username' => 'support@freeclinicdirectory.org',
					        'password' => 'conheo#1',
		   								 ),
		));
		$transport = new SmtpTransport($options);
		$transport->send($message);
		return new ViewModel(['message' => 'Email sent successfully']);
    }
}
    