<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Model\Resources;
use Application\Model\Index;


class IndexController extends AbstractActionController
{
    public function indexAction() // HOME PAGE
    {
        $data = new Index();
        $this->layout()->setVariable('myTitle', 'Free Clinics and Community Health Centers | Low Cost Dental Care');

        $this->layout()->setVariable('metaName', 'description');
        $this->layout()->setVariable('metaDesc', 'Free Clinic Directory offers listing of thousands of free and affordable clinics throughout the United States.');
        $this->layout()->setVariable('metaNameopt', 'keywords');
        $this->layout()->setVariable('metaDescopt', 'free clinics, free medical clinics, free dental care, affordable health clinics.');

        return new ViewModel(array(
             'clinics' => $data->getClinics(),
         ));

    }

    public function aboutAction()  // ABOUT PAGE
    {
     $this->layout()->setVariable('myTitle', 'About Us');
    }

    public function resourcesAction() // RESOURCES PAGE
    {
      $data = new Resources();
      $this->layout()->setVariable('myTitle', 'Free Clinic Resources &#8211; Free Clinic Directory');

        return new ViewModel(array(
             'resources' => $data->getResourcesTable(),
             'categories' => $data->getCategoriesTable(),
         ));
    }

  /*  public function headTitle($myTitle)
    {
        return "<titile>Home Page</title>";
    }*/

}
