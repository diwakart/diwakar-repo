<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Model\Addlisting;       
use Application\Form\AddlistingForm;  

class AddlistingController extends AbstractActionController
{
    public function newAction()
    {
    	$this->layout()->setVariable('myTitle', 'Add New Clinic Listing');
    	 $form = new AddlistingForm();

    	  if ($this->getRequest()->isPost())
			{
				$req = $this->getRequest();
				$data = $req->getPost();
				$name = $data['name'];
				$address = $data['address'];
				$address2 = $data['address2'];
				$city = $data['city'];
				$country = $data['country'];
				$state = $data['state'];
				$postal = $data['postal'];
				$contact_name = $data['contact_name'];
				$phone = $data['phone'];
				$fax = $data['fax'];
				$email = $data['email'];
				$website = $data['website'];
				$type = $data['type'];
				$details = $data['details'];
				$has_medical = $data['has_medical'];
				$has_dental = $data['has_dental'];
				$services = $data['services'];
				$remarks = $data['remarks'];

				$data = new Addlisting();
                $data->addClinic($name,$address,$address2,$city,$country,$state,$postal,$contact_name,$phone,$fax,$email,$website,$type,$details,$has_medical,$has_dental,$services,$remarks);

                return array('form' => $form,'message' => 'Thankyou for submitting the information.The listing will be verified before being set LIVE on FreeClinicDirectory.org');
			}
         $form->get('submit')->setValue('Submit'); 
         return array('form' => $form);
    }

   
 
}
    