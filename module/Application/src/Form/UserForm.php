<?php

 namespace Application\Form;

 use Zend\Form\Form;

 class UserForm extends Form
 {
    public function __construct($name = null)
     {
         // we want to ignore the name passed
         parent::__construct('users');

         $this->add([
             'name' => 'email',
             'type' => 'Email',
             'attributes' => [
                 'class' => 'input-style',
             ],
             'options' => [
                 'label' => 'Username (email):',
             ],
         ]);
         $this->add([
             'name' => 'password',
             'type' => 'Password',
             'attributes' => [
                 'class' => 'input-style',
             ],
             'options' => [
                 'label' => 'Password:',
            ],
         ]);
         $this->add([
             'name' => 'submit',
             'type' => 'Submit',
             'attributes' => [
                 'value' => 'Login',
                 'id' => 'submit',
                 'class' => 'input-button',
             ],
         ]);
     }
 }