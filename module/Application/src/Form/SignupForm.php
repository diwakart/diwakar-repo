<?php

 namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Application\Model\Magic;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Update;
use Zend\Db\Adapter\Adapter;
use Application\Model\Index;

 class SignupForm extends Form
 {
    protected $adapter;
    public function __construct($name = null)
     {
         // we want to ignore the name passed
        parent::__construct('signup');

        $dbdata = new Magic();
        $this->adapter = $dbdata->getDbCon();

         $this->add([
             'name' => 'email',
             'type' => 'Email',
             'attributes' => [
                 'class' => 'input-signup',
             ],
             'options' => [
                 'label' => 'Email address (will be your username):',
             ],
         ]);

          $this->add([
             'name' => 'password',
             'type' => 'Password',
             'attributes' => [
                 'class' => 'input-signup',
             ],
             'options' => [
                 'label' => 'Password:',
             ],
         ]);

          $this->add([
             'name' => 'confirmpassword',
             'type' => 'Password',
             'attributes' => [
                 'class' => 'input-signup',
             ],
             'options' => [
                 'label' => 'Retype Password:',
             ],
         ]);

          $this->add([
             'name' => 'firstname',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-signup',
             ],
             'options' => [
                 'label' => 'First Name:',
             ],
         ]);

          $this->add([
             'name' => 'lastname',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-signup',
             ],
             'options' => [
                 'label' => 'Last Name:',
             ],
         ]);

          $this->add([
             'name' => 'address',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-signup',
             ],
             'options' => [
                 'label' => 'Address:',
             ],
         ]);

          $this->add([
             'name' => 'city',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-signup',
             ],
             'options' => [
                 'label' => 'City:',
             ],
         ]);

          $select = new Element\Select('state');
          $select->setLabel('State');
          $select->setAttributes(array('class' => 'input-signup'));
          $select->setValueOptions($this->getStatesTable());
          $this->add($select);

          $this->add([
             'name' => 'zipcode',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-signup',
             ],
             'options' => [
                 'label' => 'Zip Code:',
             ],
         ]);


         $this->add([
             'name' => 'submit',
             'type' => 'Submit',
             'attributes' => [
                 'value' => 'Register',
                 'id' => 'submit',
                 'class' => 'input-signup-button',
             ],
         ]);
     }

      public function getStatesTable()
     {        
            $sql = new Sql($this->adapter);
            $select = new Select('states');
            $selectString = $sql->getSqlStringForSqlObject($select);
            $states =  $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);   
            $selectData = array();
            $selectData[''] = '-Select-';
            foreach ($states as $res) {
            $selectData[$res['id']] = $res['state_name'];
            }
            return $selectData;
     }
 }