<?php

 namespace Application\Form;

 use Zend\Form\Form;
 use Zend\Form\Element;
 use Zend\Captcha;
/* use Zend\Form\Element\Captcha;
 use Zend\Captcha\Image as CaptchaImage;*/

 class ContactForm extends Form
 {
    public function __construct($name = null)
     {
         // we want to ignore the name passed
         parent::__construct('contact');

         $this->add([
             'name' => 'name',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-contact',
                 'required' => true,
             ],
             'options' => [
                 'label' => 'Your Name:',
             ],
         ]);

         $this->add([
             'name' => 'email',
             'type' => 'Email',
             'attributes' => [
                 'class' => 'input-contact',
                  'required' => true,
             ],
             'options' => [
                 'label' => 'Your e-mail address:',
             ],
         ]);

         $this->add([
             'name' => 'subject',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-contact',
                 'required' => true,
             ],
             'options' => [
                 'label' => 'Subject:',
             ],
         ]);

         $textarea = new Element\Textarea('message');
         $textarea->setLabel('Message');
         $textarea->setAttributes(array('cols' => 15, 'rows' => 5, 'required' => true));
         $this->add($textarea);



       /*  $dirdata = './data';
 
                                         //pass captcha image options
        $captchaImage = new CaptchaImage(  array(
                'font' => $dirdata . '/fonts/arial.ttf',
                'width' => 250,
                'height' => 100,
                'dotNoiseLevel' => 40,
                'lineNoiseLevel' => 3)
        );
        $captchaImage->setImgDir($dirdata.'/captcha');
        $captchaImage->setImgUrl($urlcaptcha);
 
        //add captcha element...
        $this->add(array(
            'type' => 'Zend\Form\Element\Captcha',
            'name' => 'captcha',
            'options' => array(
                'label' => 'Please verify you are human',
                'captcha' => $captchaImage,
            ),
        ));*/



        $captcha = new Element\Captcha('captcha');
        $captcha
            ->setCaptcha(new Captcha\Dumb())
            ->setLabel('Please verify you are human');
        $this->add($captcha);
         /*--captcha code*


       /* $publickey = "6LeYvwoAAAAAAKeVsPK9aakRQfybYOOz9YmyY5jH";
        $privatekey = "6LeYvwoAAAAAAHkMQuEfHY0gTo6Q3ohEsoS9IrI4";
        $recaptcha = new Zend_Service_ReCaptcha($publickey, $privatekey);
        $captcha = new Zend_Form_Element_Captcha('challenge', 
                                        array('label'   => 'Please type each of the following words into the box below to continue', 'captcha' => 'ReCaptcha',
                                                'captchaOptions' => array('captcha' => 'ReCaptcha', 'service' => $recaptcha, 'theme' => 'clean', 'ssl'=>true)));*/
         /*-End: captcha code*/

         $this->add([
             'name' => 'submit',
             'type' => 'Submit',
             'attributes' => [
                 'value' => 'Login',
                 'id' => 'submit',
                 'class' => 'input-contact-button',
             ],
         ]);
     }
 }