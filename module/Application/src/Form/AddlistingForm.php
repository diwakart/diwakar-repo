<?php

 namespace Application\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Application\Model\Magic;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Update;
use Zend\Db\Adapter\Adapter;
use Application\Model\Index;
use Zend\Form\Element\Captcha;


 class AddlistingForm extends Form
 {
    protected $adapter;
    public function __construct($name = null,$urlcaptcha = null)
     {
         // we want to ignore the name passed
         parent::__construct('addlisting');

        $dbdata = new Magic();
        $this->adapter = $dbdata->getDbCon();

         $this->add([
             'name' => 'name',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-list',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'Clinic Name:',
             ],
         ]);

          $this->add([
             'name' => 'address',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-list',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'Address:',
             ],
         ]);

          $this->add([
             'name' => 'address2',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-list',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'Address 2:',
             ],
         ]);

          $this->add([
             'name' => 'city',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-list',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'City:',
             ],
         ]);

          $this->add([
             'name' => 'country',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-list',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'Country:',
             ],
         ]);

          $select = new Element\Select('state');
          $select->setLabel('State');
          $select->setAttributes(array('class' => 'input-list','required'=>true));
          $select->setValueOptions($this->getStatesTable());
          $this->add($select);         


          $this->add([
             'name' => 'postal',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-list',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'Zip Code:',
             ],
         ]);

         $this->add([
             'name' => 'contact_name',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-list',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'Contact Name:',
             ],
         ]);
         
         $this->add([
             'name' => 'phone',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-list',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'Contact Phone:',
             ],
         ]);

         $this->add([
             'name' => 'fax',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-list',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'Contact Fax:',
             ],
         ]);

         $this->add([
             'name' => 'email',
             'type' => 'Email',
             'attributes' => [
                 'class' => 'input-list',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'Contact Email:',
             ],
         ]);

         $this->add([
             'name' => 'website',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-list',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'Website:',
             ],
         ]);

          $select = new Element\Select('type');
          $select->setLabel('Center Type:');
          $select->setAttributes(array('class' => 'input-list','required'=>true));
          $select->setValueOptions(array(
                 '' => '-Select Center Type-',
                 'Community Health Center' => 'Community Health Center',
                 'Health Education' => 'Health Education',
                 'Health Department' => 'Health Department',
                 'Home Health' => 'Home Health',
                 'Hospital' => 'Hospital',
                 'HIV' => 'HIV',
                 'Sickle Cell' => 'Sickle Cell',
                 'Wellness' => 'Wellness',
                 'Other Primary Care Provider' => 'Other Primary Care Provider',
          ));
          $this->add($select);


          $textarea = new Element\Textarea('details');
          $textarea->setLabel('Clinic Description:');
          $textarea->setAttributes(array('cols' => 15, 'rows' => 5, 'required' => true));
          $this->add($textarea);

          $select = new Element\Select('has_medical');
          $select->setLabel('Provides Medical Care:');
          $select->setAttributes(array('class' => 'input-list','required'=>true));
          $select->setValueOptions(array(
                 '0' => 'No',
                 '1' => 'Yes',
          ));
          $select->setValue('1');
          $this->add($select);


           $select = new Element\Select('has_dental');
          $select->setLabel('Provides Dental Care:');
          $select->setAttributes(array('class' => 'input-list','required'=>true));
          $select->setValueOptions(array(
                 '0' => 'No',
                 '1' => 'Yes',
          ));
          $select->setValue('1');
          $this->add($select);

         $this->add([
             'name' => 'services',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-list',
                 'maxlength'=>'250',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'Services/Details (max 250 character):',
             ],
         ]); 

         $this->add([
             'name' => 'remarks',
             'type' => 'Text',
             'attributes' => [
                 'class' => 'input-list',
                 'maxlength'=>'250',
                 'required'=>true,
             ],
             'options' => [
                 'label' => 'Notes/Remarks (max 250 character):',
             ],
         ]); 


         /*====captcha code====*/
        $this->add([
            'type'  => 'captcha',
            'name' => 'captcha',
            'attributes' => [                                                
            ],
            'options' => [
                'label' => 'Please type each of the following words into the box below to continue',
                'captcha' => [
                    'class' => 'Figlet',
                    'wordLen' => 6,
                    'expiration' => 600,                     
                ],
            ],
        ]);

         /*====End:captcha code====*/


         $this->add([
             'name' => 'submit',
             'type' => 'Submit',
             'attributes' => [
                 'value' => 'Submit',
                 'id' => 'submit',
                 'class' => 'input-addlist-button',
             ],
         ]);
     }
     public function getStatesTable()
     {        
            $sql = new Sql($this->adapter);
            $select = new Select('states');
            $selectString = $sql->getSqlStringForSqlObject($select);
            $states =  $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);   
            $selectData = array();
            $selectData[''] = '-Select-';
            foreach ($states as $res) {
            $selectData[$res['id']] = $res['state_name'];
            }
            return $selectData;
     }
 }