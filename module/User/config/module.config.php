<?php 
    
return [
    'service_manager' => [
        'factories' => [
            \Zend\Authentication\AuthenticationService::class 
                => Service\Factory\AuthenticationServiceFactory::class,
            // ...
        ],
    ],
];